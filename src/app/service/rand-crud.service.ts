import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { randonner } from '../models/randonner';


const baseUrl = 'http://localhost:8080/rand';
@Injectable({
  providedIn: 'root'
})
export class RandCrudService { constructor(private http: HttpClient) { }

public getRandonners(): Observable<randonner[]> {
  return this.http.get<randonner[]>(`${baseUrl}/all`);
}

get(id: number): Observable<any> {
  return this.http.get(`${baseUrl}/${id}`);
}


public addRandonner(randonner: randonner): Observable<any> {
return this.http.post<randonner>(`${baseUrl}/add`, randonner);
}

public updateRandonner(randonner: randonner): Observable<randonner> {
return this.http.put<randonner>(`${baseUrl}`, randonner);
}

public deleteRandonner(id: any): Observable<void> {
return this.http.delete<void>(`${baseUrl}/${id}`);
}

}
