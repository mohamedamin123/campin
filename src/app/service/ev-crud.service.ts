import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { evenment } from '../models/evenment';

const baseUrl = 'http://localhost:8080/event';
@Injectable({
  providedIn: 'root'
})
export class EvCrudService {

  constructor(private http: HttpClient) { }

  public getEvenments(): Observable<evenment[]> {
    return this.http.get<evenment[]>(`${baseUrl}/all`);
  }

  get(id: number): Observable<any> {
    return this.http.get(`${baseUrl}/${id}`);
  }
 
  
public addEvenment(evenment: evenment): Observable<any> {
  return this.http.post<evenment>(`${baseUrl}/add`, evenment);
}

public updateEvenment(evenment: evenment): Observable<evenment> {
  return this.http.put<evenment>(`${baseUrl}`, evenment);
}

public deleteEvenment(id: any): Observable<void> {
  return this.http.delete<void>(`${baseUrl}/${id}`);
}

}
