import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Annonce } from '../models/annonce.model';

const baseUrl = 'http://localhost:8080/annonce';

@Injectable({
  providedIn: 'root'
})
export class AnnonceService {
  

constructor(private http: HttpClient) {}

  public getAnnonces(): Observable<Annonce[]> {
    return this.http.get<Annonce[]>(`${baseUrl}/all`);
  }

  get(id: number): Observable<any> {
    return this.http.get(`${baseUrl}/${id}`);
  }
 
  
public addAnnonce(annonce: Annonce): Observable<Annonce> {
  return this.http.post<Annonce>(`${baseUrl}/add`, annonce);
}

public updateAnnonce(annonce: Annonce): Observable<Annonce> {
  return this.http.put<Annonce>(`${baseUrl}`, annonce);
}

public deleteAnnonce(id: any): Observable<void> {
  return this.http.delete<void>(`${baseUrl}/${id}`);
}

}
