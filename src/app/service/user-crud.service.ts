import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { utilisateur } from '../models/utilisateur.model';

const baseUrl = 'http://localhost:8080/utilisateur';
@Injectable({
  providedIn: 'root'
})
export class UserCRUDService {

  public roles!:string[];

  constructor(private http: HttpClient) { }

  getAll(): Observable<utilisateur[]> {
    return this.http.get<utilisateur[]>(baseUrl+"/all");
  }

  get(id: any): Observable<utilisateur> {
    return this.http.get(`${baseUrl}/${id}`);
  }


  update(data: any): Observable<any> {
    return this.http.put(baseUrl , data);
  }

  delete(id: BigInteger): Observable<any> {
    return this.http.delete(`${baseUrl}/${id}`);
  }

  isAdmin():Boolean{
    if (!this.roles) //this.roles== undefiened
    return false;
    return (this.roles.indexOf('ADMIN') >-1) ;
    }

    public getUsers(): Observable<utilisateur[]> {
      return this.http.get<utilisateur[]>(`${baseUrl}/all`);
    }

  public addUser(utilisateur: utilisateur): Observable<utilisateur> {
    return this.http.post<utilisateur>(`${baseUrl}/add`, utilisateur);
  }

  public updateUser(utilisateur: utilisateur): Observable<utilisateur> {
    return this.http.put<utilisateur>(`${baseUrl}/update`, utilisateur);
  }

  public deleteUser(id: BigInteger): Observable<void> {
    return this.http.delete<void>(`${baseUrl}/${id}`);
  }

  //deleteAll(): Observable<any> {
 //   return this.http.delete(baseUrl);
 // }

 // findByTitle(title: any): Observable<utilisateur[]> {
  //  return this.http.get<utilisateur[]>(`${baseUrl}?title=${title}`);
 // }
}
