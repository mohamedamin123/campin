import { utilisateur } from 'src/app/models/utilisateur.model';
import { UserCRUDService } from 'src/app/service/user-crud.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Form, NgForm } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { registerService } from 'src/app/service/register.service';
import { TokenStorageService } from 'src/app/service/token-storage.service';
import { roles } from 'src/app/models/roles.modul';
@Component({
  selector: 'app-all-utilisateurs',
  templateUrl: './all-utilisateurs.component.html',
  styleUrls: ['./all-utilisateurs.component.css']
})
export class AllUtilisateursComponent implements OnInit {
  currentUser: any;
  public utilisateurs?: utilisateur[];
  public edituser?: utilisateur;
  public deleteuser?: utilisateur;
  form: any = {};
  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = '';
  constructor( private registerService: registerService ,private UserCRUDService: UserCRUDService,private token: TokenStorageService ) { }

  ngOnInit(): void {
    this.currentUser = this.token.getUser();
    this.getUsers();
   
  }


  public getUsers(): void {
    this.UserCRUDService.getUsers().subscribe(
      (response: utilisateur[]) => {
        this.utilisateurs = response;
        console.log(this.utilisateurs);
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }
  
  public onAddUser(addForm: NgForm): void {
    document.getElementById('add-employee-form')!.click();
    this.registerService.register(this.form).subscribe(
      data => {
        console.log(data);
        this.getUsers();
        addForm.reset();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
        addForm.reset();
      }
    );
  }

  public onUpdateUser(utilisateur: utilisateur ): void {
    this.UserCRUDService.updateUser(utilisateur).subscribe(
      (response: utilisateur) => {
        console.log(response);
        this.getUsers();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public onDeleteUser(id: BigInteger): void {
    this.UserCRUDService.deleteUser(id).subscribe(
      (response: void) => {
        console.log(response);
        this.getUsers();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public onOpenModal(utilisateur: utilisateur, mode: string): void {
    const container = document.getElementById('main-container');
    const button = document.createElement('button');
    button.type = 'button';
    button.style.display = 'none';
    button.setAttribute('data-toggle', 'modal');
    if (mode === 'add') {
      button.setAttribute('data-target', '#addEmployeeModal');
    }
    if (mode === 'edit') {
      this.edituser = utilisateur;
      button.setAttribute('data-target', '#updateEmployeeModal');
    }
    if (mode === 'delete') {
      this.deleteuser = utilisateur;
      button.setAttribute('data-target', '#deleteEmployeeModal');
    }
    container!.appendChild(button);
    button.click();
  }
}
