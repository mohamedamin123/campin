import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AllRondRoutingModule } from './all-rond-routing.module';
import { AllRondComponent } from './all-rond/all-rond.component';


@NgModule({
  declarations: [
    AllRondComponent
  ],
  imports: [
    CommonModule,
    AllRondRoutingModule
  ]
})
export class AllRondModule { }
