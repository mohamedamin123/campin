import { evenment } from './../../../../models/evenment';
import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { EvCrudService } from 'src/app/service/ev-crud.service';
import { TokenStorageService } from 'src/app/service/token-storage.service';

@Component({
  selector: 'app-all-event',
  templateUrl: './all-event.component.html',
  styleUrls: ['./all-event.component.css']
})
export class AllEventComponent implements OnInit {

  evenments!:evenment[];
  public editEvenment!: evenment;
  public deleteEvenment!: evenment;

 
  
  currentIndex = -1;
  private roles : string[] = [];
  isLoggedIn = true;
  showAdminBoard = false;
  showRandonneurBoard = false;

  constructor(private anse:EvCrudService,public dialog: MatDialog ,private tokenStorageService: TokenStorageService , private route:Router) { }

  
  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenStorageService.getToken();
    if (this.isLoggedIn) {
      const user = this.tokenStorageService.getUser();
      this.roles = user.roles;
      this.showRandonneurBoard = this.roles.includes('RANDONNEUR');
    this.getEvenments();
    }
  }

  public getEvenments(): void {
    this.anse.getEvenments().subscribe(
      (response: evenment[]) => {
        this.evenments = response;
        console.log(this.evenments);
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  
  public onAddEvenment(addForm: NgForm): void {
    document.getElementById('add-employee-form')!.click();
    this.anse.addEvenment(addForm.value).subscribe(
      (response: evenment) => {
        console.log(response);
        this.getEvenments();
        addForm.reset();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
        addForm.reset();
      }
    );
  }

  public onUpdateEvenment(Evenment: evenment): void {
    this.anse.updateEvenment(Evenment).subscribe(
      (response: evenment) => {
        console.log(response);
        this.getEvenments();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }
  

  public onDeleteEvennment(annonceId: any): void {
    this.anse.deleteEvenment(annonceId).subscribe(
      (response: void) => {
        console.log(response);
        this.getEvenments();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }
  

  public onOpenModal(evenment: evenment, mode: string): void {
    const container = document.getElementById('main-container');
    const button = document.createElement('button');
    button.type = 'button';
    button.style.display = 'none';
    button.setAttribute('data-toggle', 'modal');
    if (mode === 'add') {
      button.setAttribute('data-target', '#addEmployeeModal');
    }
    if (mode === 'edit') {
      this.editEvenment = evenment;
      button.setAttribute('data-target', '#updateEmployeeModal');
    }
    if (mode === 'delete') {
      this.deleteEvenment = evenment;
      button.setAttribute('data-target', '#deleteEmployeeModal');
    }
    container!.appendChild(button);
    button.click();
  }


  
}

