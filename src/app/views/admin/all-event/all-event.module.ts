import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AllEventRoutingModule } from './all-event-routing.module';
import { AllEventComponent } from './all-event/all-event.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    AllEventComponent
  ],
  imports: [
    FormsModule,CommonModule,
    AllEventRoutingModule
  ]
})
export class AllEventModule { }
