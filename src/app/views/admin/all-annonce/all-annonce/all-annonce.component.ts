import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Annonce } from 'src/app/models/annonce.model';
import { AnnonceService } from 'src/app/service/annonce.service';

@Component({
  selector: 'app-all-annonce',
  templateUrl: './all-annonce.component.html',
  styleUrls: ['./all-annonce.component.css']
})
export class AllAnnonceComponent implements OnInit {

  public annonces?: Annonce[];
  public editannonce?: Annonce;
  public deleteannonce?: Annonce;
  form: any = {};
  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = '';
  constructor( private annservice: AnnonceService  ) { }

  ngOnInit(): void {
    this.getAnnonces();
  }


  public getAnnonces(): void {
    this.annservice.getAnnonces().subscribe(
      (response: Annonce[]) => {
        this.annonces = response;
        console.log(this.annonces);
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }
  
  public onAddAnnonce(addForm: NgForm): void {
    document.getElementById('add-employee-form')!.click();
    this.annservice.addAnnonce(this.form).subscribe(
      data => {
        console.log(data);
        this.getAnnonces();
        addForm.reset();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
        addForm.reset();
      }
    );
  }

  public onUpdateAnnonce(Annonce: Annonce): void {
    this.annservice.updateAnnonce(Annonce).subscribe(
      (response: Annonce) => {
        console.log(response);
        this.getAnnonces();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public onDeleteAnnonce(id: any): void {
    this.annservice.deleteAnnonce(id).subscribe(
      (response: void) => {
        console.log(response);
        this.getAnnonces();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public onOpenModal(Annonce: Annonce, mode: string): void {
    const container = document.getElementById('main-container');
    const button = document.createElement('button');
    button.type = 'button';
    button.style.display = 'none';
    button.setAttribute('data-toggle', 'modal');
    if (mode === 'add') {
      button.setAttribute('data-target', '#addEmployeeModal');
    }
    if (mode === 'edit') {
      this.editannonce = Annonce;
      button.setAttribute('data-target', '#updateEmployeeModal');
    }
    if (mode === 'delete') {
      this.deleteannonce = Annonce;
      button.setAttribute('data-target', '#deleteEmployeeModal');
    }
    container!.appendChild(button);
    button.click();
  }
}
