import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AllAnnonceRoutingModule } from './all-annonce-routing.module';
import { AllAnnonceComponent } from './all-annonce/all-annonce.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [
    AllAnnonceComponent
  ],
  imports: [
    CommonModule,
    AllAnnonceRoutingModule,
    FormsModule,
    HttpClientModule,
  ]
})
export class AllAnnonceModule { }
