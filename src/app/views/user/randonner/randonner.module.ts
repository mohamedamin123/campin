import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RandonnerRoutingModule } from './randonner-routing.module';
import { RandonnerComponent } from './randonner/randonner.component';
import {MatDialogModule} from '@angular/material/dialog';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatRadioModule } from '@angular/material/radio';
import { MatStepperModule } from '@angular/material/stepper';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { ParticipeComponent } from './participe/participe.component';




@NgModule({
  declarations: [
    RandonnerComponent,
    ParticipeComponent,
  ],
  imports: [
    CommonModule,
RandonnerRoutingModule,
    MatDialogModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatRadioModule,
    MatButtonModule,
    FormsModule,
    MatStepperModule,
    ReactiveFormsModule,
    MatNativeDateModule,
    MatDatepickerModule,
    FormsModule
  ]
})
export class RandonnerModule { }
