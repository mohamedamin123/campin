import { randonner } from './../../../../models/randonner';
import { RandCrudService } from './../../../../service/rand-crud.service';


import { Component, OnInit } from '@angular/core';

import { HttpErrorResponse } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { TokenStorageService } from 'src/app/service/token-storage.service';
import { DialogContentComponent } from '../../events/dialog-content/dialog-content.component';
import { ParticipeComponent } from '../participe/participe.component';




@Component({
  selector: 'app-randonner',
  templateUrl: './randonner.component.html',
  styleUrls: ['./randonner.component.css']
})
export class RandonnerComponent implements OnInit {
 
 randonners!:randonner[];
  public editRandonner!: randonner;
  public deleteRandonner!: randonner;

  private roles : string[] = [];
  isLoggedIn = true;
  showAdminBoard = false;
  showRandonneurBoard = false;

  constructor(public dialog: MatDialog,private anse:RandCrudService,private tokenStorageService: TokenStorageService ) { }

  
  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenStorageService.getToken();
    if (this.isLoggedIn) {
      const user = this.tokenStorageService.getUser();
      this.roles = user.roles;
      this.showRandonneurBoard = this.roles.includes('RANDONNEUR');
    this.getRandonners();
    }
  }

  public getRandonners(): void {
    this.anse.getRandonners().subscribe(
      (response: randonner[]) => {
        this.randonners = response;
        console.log(this.randonners);
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  
  public onAddRandonner(addForm: NgForm): void {
    document.getElementById('add-employee-form')!.click();
    this.anse.addRandonner(addForm.value).subscribe(
      (response: randonner) => {
        console.log(response);
        this.getRandonners();
        addForm.reset();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
        addForm.reset();
      }
    );
  }

  
  public onUpdateRandonner(Randonner: randonner): void {
    this.anse.updateRandonner(Randonner).subscribe(
      (response: randonner) => {
        console.log(response);
        this.getRandonners();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }
  

  public onDeleteRandonner(annonceId: any): void {
    this.anse.deleteRandonner(annonceId).subscribe(
      (response: void) => {
        console.log(response);
        this.getRandonners();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }
  
  public onOpenModal(randonner:randonner, mode: string): void {
    const container = document.getElementById('main-container');
    const button = document.createElement('button');
    button.type = 'button';
    button.style.display = 'none';
    button.setAttribute('data-toggle', 'modal');
    if (mode === 'add') {
      button.setAttribute('data-target', '#addEmployeeModal');
    }
    if (mode === 'edit') {
      this.editRandonner = randonner;
      button.setAttribute('data-target', '#updateEmployeeModal');
    }
    if (mode === 'delete') {
      this.deleteRandonner = randonner;
      button.setAttribute('data-target', '#deleteEmployeeModal');
    }
    container!.appendChild(button);
    button.click();
  }
  
 
  
  openDiagg() {
    const dialogRef = this.dialog.open(ParticipeComponent);

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }
 
}


