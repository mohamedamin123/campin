import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { Annonce } from 'src/app/models/annonce.model';
import { randonner } from 'src/app/models/randonner';
import { AnnonceService } from 'src/app/service/annonce.service';
import { RandCrudService } from 'src/app/service/rand-crud.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {
  randonners!:randonner[];
  annonces!:Annonce[];
  constructor(private anse:AnnonceService) { }

  ngOnInit(): void {
    
  }
  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: true,
    dots: true,
    navSpeed: 700,
    navText: ['', ''],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 2
      },
      740: {
        items: 3
      },
      940: {
        items: 1
      }
    },
    nav: true

  }

  public getAnnonces(): void {
    this.anse.getAnnonces().subscribe(
      (response: Annonce[]) => {
        this.annonces = response;
        console.log(this.annonces);
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }
}
