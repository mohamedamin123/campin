import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Annonce } from 'src/app/models/annonce.model';
import { utilisateur } from 'src/app/models/utilisateur.model';
import { AnnonceService } from 'src/app/service/annonce.service';
import { CartService } from 'src/app/service/cart.service';
import { TokenStorageService } from 'src/app/service/token-storage.service';
import { DetailsAnnComponent } from '../details-ann/details-ann.component';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.css']
})
export class ShopComponent implements OnInit {


  annonces!:Annonce[];
  public editAnnonce!: Annonce;
  public deleteAnnonce!: Annonce;

  private roles : string[] = [];
  isLoggedIn = true;
  showAdminBoard = false;
  showRandonneurBoard = false;

  constructor(private anse: AnnonceService,public dialog: MatDialog ,private tokenStorageService: TokenStorageService , private route:Router,private cartService:CartService) { }

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenStorageService.getToken();
    if (this.isLoggedIn) {
      const user = this.tokenStorageService.getUser();
      this.roles = user.roles;
      this.showRandonneurBoard = this.roles.includes('RANDONNEUR');
    this.getAnnonces();
    }
  }

  addtocart(annonce: any){
    this.cartService.addtoCart(annonce);
  }


  
  public getAnnonces(): void {
    this.anse.getAnnonces().subscribe(
      (response: Annonce[]) => {
        this.annonces = response;
        console.log(this.annonces);
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public onAddAnnonce(addForm: NgForm): void {
    document.getElementById('add-employee-form')!.click();
    this.anse.addAnnonce(addForm.value).subscribe(
      (response: Annonce) => {
        console.log(response);
        this.getAnnonces();
        addForm.reset();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
        addForm.reset();
      }
    );
  }

  public onUpdateAnnonce(annonce: Annonce): void {
    this.anse.updateAnnonce(annonce).subscribe(
      (response: Annonce) => {
        console.log(response);
        this.getAnnonces();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public onDeleteAnnonce(annonceId: any): void {
    this.anse.deleteAnnonce(annonceId).subscribe(
      (response: void) => {
        console.log(response);
        this.getAnnonces();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }
  


  public onOpenModal(annonce: Annonce, mode: string): void {
    const container = document.getElementById('main-container');
    const button = document.createElement('button');
    button.type = 'button';
    button.style.display = 'none';
    button.setAttribute('data-toggle', 'modal');
    if (mode === 'add') {
      button.setAttribute('data-target', '#addEmployeeModal');
    }
    if (mode === 'edit') {
      this.editAnnonce = annonce;
      button.setAttribute('data-target', '#updateEmployeeModal');
    }
    if (mode === 'delete') {
      this.deleteAnnonce = annonce;
      button.setAttribute('data-target', '#deleteEmployeeModal');
    }
    container!.appendChild(button);
    button.click();
  }

  
  openDialog(){
    const dialogRef = this.dialog.open(DetailsAnnComponent);
    width:"30%"
  }
}

