import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AnnonceService } from 'src/app/service/annonce.service';

@Component({
  selector: 'app-details-ann',
  templateUrl: './details-ann.component.html',
  styleUrls: ['./details-ann.component.css']
})
export class DetailsAnnComponent implements OnInit {

 
  id!:any ;
  annonce: any;

  constructor(private anse: AnnonceService,private route: ActivatedRoute) { }



  ngOnInit(): void {
  
    this.get(this.route.snapshot.params.id);
  }
  get(id: number): void {
    this.anse.get(id)
      .subscribe(
        data => {
          this.annonce = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }
  }
