import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContacterRoutingModule } from './contacter-routing.module';
import { ContacterComponent } from './contacter/contacter.component';


@NgModule({
  declarations: [
    ContacterComponent
  ],
  imports: [
    CommonModule,
    ContacterRoutingModule
  ]
})
export class ContacterModule { }
