import { Component, OnInit } from '@angular/core';
import { TokenStorageService } from 'src/app/service/token-storage.service';

@Component({
  selector: 'app-edit-profil',
  templateUrl: './edit-profil.component.html',
  styleUrls: ['./edit-profil.component.css']
})
export class EditProfilComponent implements OnInit {
 
  rolelist: string[] = ['USER', 'RANDONNEUR', 'ADMIN'];
  currentUser: any;

  constructor() { }
  ngOnInit(): void {
  }

  reloadPage(): void {
    window.location.reload();
  }
}
