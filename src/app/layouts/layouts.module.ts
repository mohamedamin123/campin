import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminlayoutComponent } from './adminlayout/adminlayout.component';
import { UserlayoutComponent } from './userlayout/userlayout.component';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import {MatIconModule} from '@angular/material/icon';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel';
@NgModule({
  declarations: [
    AdminlayoutComponent,
    UserlayoutComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    MatIconModule
  ]
})
export class LayoutsModule { }
